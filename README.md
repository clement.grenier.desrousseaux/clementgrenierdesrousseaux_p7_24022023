# Installation without Docker

### Clone the project and install dependencies

```bash
git clone https://gitlab.com/clement.grenier.desrousseaux/clementgrenierdesrousseaux_p7_24022023.git
cd clementgrenierdesrousseaux_p7_24022023
composer install
touch .env.local
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

Add in .env.local file :

```
DATABASE_URL="mysql://!username!:!password!@localhost:3306/app?serverVersion=8&charset=utf8mb4"

###> lexik/jwt-authentication-bundle ###

JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=YOUR_PASSPHRASE

###< lexik/jwt-authentication-bundle ###

```

### Create the database and load fixtures

```bash
php bin/console doctrine:database:create
php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
symfony serve
```

# Installation with Docker

### Clone the project

```bash
git clone https://gitlab.com/clement.grenier.desrousseaux/clementgrenierdesrousseaux_p7_24022023.git
```

### Install dependencies and configure the project and the security
```bash
cd clementgrenierdesrousseaux_p7_24022023
docker-compose up -d
docker exec -it myapp_p7 bash
composer install
touch .env.local
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

Add in .env.local file :

```
DATABASE_URL="mysql://root:password@database:3306/app?serverVersion=8&charset=utf8mb4"

###> lexik/jwt-authentication-bundle ###

JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=YOUR_PASSPHRASE

###< lexik/jwt-authentication-bundle ###

```

### Init the database and load fixtures

```bash
symfony console make:migration
symfony console doctrine:migrations:migrate
symfony console doctrine:fixtures:load  
symfony serve
```

# API Documentation

Without Docker : 
```
http://localhost:8000/api/doc
```

With Docker : 
```
http://localhost:9000/api/doc
```

