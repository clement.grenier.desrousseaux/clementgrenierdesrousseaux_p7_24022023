<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class ProductController extends AbstractController
{
    /**
     * Get list of all products
     * @param ProductRepository $productRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[Route('/api/products', name: 'app_products', methods: ['GET'])]
    public function index(ProductRepository $productRepository, SerializerInterface $serializer, TagAwareCacheInterface $cache): JsonResponse
    {
        $idCache = "getProducts";
        $productsList = $cache->get($idCache, function (ItemInterface $item) use ($productRepository) {
           $item->tag('getProducts');
           return $productRepository->findAll();
        });


        $products = $serializer->serialize($productsList, 'json', ['groups' => 'getProducts']);

        return new JsonResponse(
            $products,
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * Get product information by identifier
     * @param ProductRepository $productRepository
     * @param int $productIdentifier
     * @param SerializerInterface $serializer
     * @param UrlGeneratorInterface $urlGenerator
     * @return JsonResponse
     */
    #[Route('/api/product/{productIdentifier}', name: 'app_product_by_identifier', methods: ['GET'])]
    public function getProductById(ProductRepository $productRepository, int $productIdentifier, SerializerInterface $serializer, UrlGeneratorInterface $urlGenerator): JsonResponse
    {
        $product = $productRepository->find($productIdentifier);

        if ($product === false) {
            return new JsonResponse('Product not found', Response::HTTP_NOT_FOUND);
        }

        $product = $serializer->serialize($product, 'json', ['groups' => 'getProducts']);

        $location = [];
        $location['read'] = $urlGenerator->generate('app_product_by_identifier', ['productIdentifier' => $productIdentifier], UrlGeneratorInterface::ABSOLUTE_URL);
        $location['update'] = $urlGenerator->generate('app_product_by_identifier', ['productIdentifier' => $productIdentifier], UrlGeneratorInterface::ABSOLUTE_URL);
        $location['delete'] = $urlGenerator->generate('app_product_by_identifier', ['productIdentifier' => $productIdentifier], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse(
            $product,
            Response::HTTP_OK,
            [
                "Location" => json_encode($location),
            ],
            true
        );
    }
}
