<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    /**
     * Get list of all users
     * @param UserRepository $userRepository
     * @param SerializerInterface $serializer
     * @param UrlGeneratorInterface $urlGenerator
     * @return JsonResponse
     */
    #[Route('/api/users', name: 'app_users', methods: ['GET'])]
    public function index(UserRepository $userRepository, SerializerInterface $serializer, UrlGeneratorInterface $urlGenerator): JsonResponse
    {
        $actualUser = $this->getUser()->getUserIdentifier();
        $actualUser = $userRepository->findOneBy(['email' => $actualUser]);

        $users = $userRepository->findBy(['company' => $actualUser->getCompany()->getId()]);
        $usersToSend = $serializer->serialize($users, 'json', ['groups' => 'getUser']);
        $location = [];
        $location['create'] = $urlGenerator->generate('app_create_user', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $location = json_encode($location);

        return new JsonResponse(
            $usersToSend,
            Response::HTTP_OK,
            [
                'Location' => $location
            ],
            true
        );
    }

    /**
     * Get a user by identifier
     * @param UserRepository $userRepository
     * @param int $userIdentifier
     * @param SerializerInterface $serializer
     * @param UrlGeneratorInterface $urlGenerator
     * @return JsonResponse
     */
    #[Route('/api/user/{userIdentifier}', name: 'app_user_by_identifier', methods: ['GET'])]
    public function getUserById(UserRepository $userRepository, int $userIdentifier, SerializerInterface $serializer, UrlGeneratorInterface $urlGenerator): JsonResponse
    {
        $userToSend = $userRepository->find($userIdentifier);

        if ($userToSend === false) {
            return new JsonResponse(
                "L'utilisateur n'existe pas",
                Response::HTTP_NOT_FOUND,
                [],
                true
            );
        }

        $actualUser = $this->getUser()->getUserIdentifier();
        $actualUser = $userRepository->findOneBy(['email' => $actualUser]);

        if ($actualUser->getCompany()->getCompanyName() !== $userToSend->getCompany()->getCompanyName()) {
            return new JsonResponse(
                "Vous n'avez pas accès à cet utilisateur",
                Response::HTTP_FORBIDDEN,
                [],
                true
            );
        }

        $userToSend = $serializer->serialize($userToSend, 'json', ['groups' => 'getUser']);

        $location = [];
        $location['delete'] = $urlGenerator->generate('app_delete_user_by_identifier', ['userIdentifier' => $userIdentifier], UrlGeneratorInterface::ABSOLUTE_URL);
        $location = json_encode($location);

        return new JsonResponse(
            $userToSend,
            200,
            [
                "Location" => $location
            ],
            true
        );
    }

    /**
     * Delete a user by identifier
     * @param UserRepository $userRepository
     * @param int $userIdentifier
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     */
    #[Route('/api/user/{userIdentifier}', name: 'app_delete_user_by_identifier', methods: ['DELETE'])]
    public function deleteUser(UserRepository $userRepository, int $userIdentifier, EntityManagerInterface $manager): JsonResponse
    {

        $userToDelete = $userRepository->find($userIdentifier);

        if ($userToDelete === null) {
            return new JsonResponse(
                "L'utilisateur n'existe pas",
                Response::HTTP_NOT_FOUND,
                [],
                true
            );
        }

        $actualUser = $this->getUser()->getUserIdentifier();
        $actualUser = $userRepository->findOneBy(['email' => $actualUser]);

        if ($actualUser->getCompany()->getCompanyName() !== $userToDelete->getCompany()->getCompanyName()) {
            return new JsonResponse(
                "Vous ne pouvez pas supprimer un utilisateur d'une autre entreprise",
                Response::HTTP_FORBIDDEN,
                [],
                true
            );
        }

        $manager->remove($userToDelete);
        $manager->flush();

        return new JsonResponse(
            "L'utilisateur a été supprimé",
            Response::HTTP_NO_CONTENT,
            [],
            true
        );

    }

    /**
     * Add a user
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param CompanyRepository $repository
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param UrlGeneratorInterface $urlGenerator
     * @return JsonResponse
     */
    #[Route('/api/users', name: 'app_create_user', methods: ['POST'])]
    public function addUser(UserRepository $userRepository, EntityManagerInterface $manager, Request $request, CompanyRepository $repository, UserPasswordHasherInterface $userPasswordHasher, UrlGeneratorInterface $urlGenerator): JsonResponse
    {

        $content = $request->toArray();

        $actualUser = $this->getUser()->getUserIdentifier();
        $actualUser = $userRepository->findOneBy(['email' => $actualUser]);

        if ($actualUser->getCompany()->getCompanyName() !== $content['company']) {
            return new JsonResponse(
                "Vous ne pouvez pas créer un utilisateur pour une autre entreprise",
                Response::HTTP_FORBIDDEN,
                [],
                true
            );
        }

        $user = new User();
        $user->setEmail($content['email']);
        $user->setPassword($userPasswordHasher->hashPassword($user, $content['password']));

        $company = $repository->findBy(['companyName' => $content['company']]);

        $user->setCompany($company[0]);

        $manager->persist($user);
        $manager->flush();

        $location = [];
        $location['read'] = $urlGenerator->generate('app_user_by_identifier', ['userIdentifier' => $user->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $location['delete'] = $urlGenerator->generate('app_delete_user_by_identifier', ['userIdentifier' => $user->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $location = json_encode($location);

        return new JsonResponse(
            'User added',
            Response::HTTP_CREATED,
            [
                'Location' => $location
            ],
            true
        );


    }
}
