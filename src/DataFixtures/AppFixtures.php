<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function load(ObjectManager $manager): void
    {

        $company1 = new Company();
        $company1->setCompanyName("Fnac");
        $manager->persist($company1);

        $company2 = new Company();
        $company2->setCompanyName("Darty");
        $manager->persist($company2);

        $company3 = new Company();
        $company3->setCompanyName("Boulanger");
        $manager->persist($company3);


        $user = new User();
        $user->setEmail("test@test.fnac");
        $user->setRoles(["ROLE_USER"]);
        $user->setPassword($this->userPasswordHasher->hashPassword($user, "password"));
        $user->setCompany($company1);
        $manager->persist($user);

        $user2 = new User();
        $user2->setEmail("test@test.darty");
        $user2->setRoles(["ROLE_USER"]);
        $user2->setPassword($this->userPasswordHasher->hashPassword($user2, "password"));
        $user2->setCompany($company2);
        $manager->persist($user2);

        $user3 = new User();
        $user3->setEmail("test@test.boulanger");
        $user3->setRoles(["ROLE_USER"]);
        $user3->setPassword($this->userPasswordHasher->hashPassword($user3, "password"));
        $user3->setCompany($company3);
        $manager->persist($user3);

        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName("Product-$i-fnac");
            $product->setDescription("Description-$i-fnac");
            $product->setPrice(mt_rand(1, 200));
            $manager->persist($product);
        }

        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName("Product-$i-darty");
            $product->setDescription("Description-$i-darty");
            $product->setPrice(mt_rand(1, 200));
            $manager->persist($product);
        }

        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName("Product-$i-boulanger");
            $product->setDescription("Description-$i-boulanger");
            $product->setPrice(mt_rand(1, 200));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
